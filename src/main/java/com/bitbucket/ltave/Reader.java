package com.bitbucket.ltave;

import javafx.application.Application;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;


public abstract class Reader extends Application {
    /**
     * Reads the given page
     * @param page sets the current page
     */
    abstract void read(int page);

    /**
     * Reads the next page
     * @throws IOException
     */
    abstract void nextPage() throws IOException;

    /**
     * Reads the previous page
     * @throws IOException
     */
    abstract void previousPage() throws IOException;

    /**
     * Opens the book located in path
     * @param path book's path
     * @throws IOException
     */
    abstract void open(String path) throws IOException;

    /**
     * Closes the opened book
     * @throws IOException
     */
    abstract void close() throws IOException;

    protected Stage primaryStage;
    protected List page;
    protected Integer currentPage;
    protected String title;

    /**
     * Intercepts and handles the clicks event.
     *
     * If it captures a primary click it proceeds
     * on the next page,if it captures a secondary
     * click it goes the previous page
     */
    protected void setClickEvent() {
        primaryStage.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
            try {
                switch (event.getButton()) {
                    case PRIMARY:
                        nextPage();
                        break;
                    case SECONDARY:
                        previousPage();
                }

            } catch (IOException e) {
                e.printStackTrace();

            }
        });
    }

    /**
     * Intercepts and handles keyboard input.
     *
     * It goes on the next page if it captures
     * PAGE DOWN or RIGHT ARROW.
     *
     * It goes on the previous page if it captures
     * PAGE UP or LEFT ARROW.
     *
     * It goes on the first page if it captures the
     * HOME button.
     *
     * It goes on the last page if it captures the
     * END button.
     */
    protected void setKeyEvent() {
        primaryStage.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            try {
                switch (event.getCode()) {
                    case KP_UP:
                    case UP:
                        previousPage();
                        break;
                    case KP_DOWN:
                    case DOWN:
                        nextPage();
                        break;
                    case KP_LEFT:
                    case LEFT:
                        previousPage();
                        break;
                    case KP_RIGHT:
                    case RIGHT:
                        nextPage();
                        break;
                    case PAGE_DOWN:
                        nextPage();
                        break;
                    case PAGE_UP:
                        previousPage();
                        break;
                    case HOME:
                        currentPage = 0;
                        read(currentPage);
                        break;
                    case END:
                        currentPage = page.size() - 1;
                        read(currentPage);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Intercepts and handles the scroll event.
     *
     * Scroll down: next page.
     * Scroll up: previous page
     */
    protected void setScrollEvent() {
        primaryStage.addEventFilter(ScrollEvent.SCROLL, event -> {
            try {
                int dy = (int) event.getDeltaY();
                if (dy < 0)
                    nextPage();
                else
                    previousPage();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Updates the window title
     */
    protected void updateTitle() {
        this.primaryStage.setTitle(title + " (" +
                currentPage.toString() + ")");
    }

    /**
     * Extracts metadata from the book.
     *
     * @param path identifies the book
     * @return book with the extracted information
     */
    abstract public Book extractInformation(String path);
}
