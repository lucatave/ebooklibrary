package com.bitbucket.ltave;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Entity class for table book
 */
@DatabaseTable(tableName = "book")
public class Book {
    @DatabaseField(canBeNull = false, useGetSet = true, uniqueCombo = true)
    private String author;

    @DatabaseField(canBeNull = false, useGetSet = true, uniqueCombo = true)
    private String title;

    @DatabaseField(useGetSet = true, dataType = DataType.DATE_STRING)
    private Date date;

    @DatabaseField(id = true, useGetSet = true)
    private String path;

    @DatabaseField(useGetSet = true, canBeNull = true)
    private Integer bookPages;

    /**
     * Creates a new book
     * @param author book's author
     * @param title book's title
     * @param date book's date
     * @param path book's path
     * @param bookPages book's number of pages
     */
    public Book(String author, String title, Date date, String path, Integer bookPages) {
        this.author = author;
        this.title = title;
        this.date = date;
        this.path = path;
        this.bookPages = bookPages;
    }

    /**
     * Default constructor required by ORMLite
     */
    public Book() {
    }

    /**
     * Return the book's author
     * @return book's author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Set the book's author
     * @param author new book's author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Returns the book's publication date
     * @return book's date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set the book's publication date
     * @param date new book's publication date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Returns the book's location
     * @return book's path
     */
    public String getPath() {
        return path;
    }

    /**
     * Set the book's location
     * @param path book's location
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Return the number of pages
     * @return number of pages
     */
    public Integer getBookPages() {
        return bookPages;
    }

    /**
     * Set the number of pages
     * @param bookPages number of pages to be set
     */
    public void setBookPages(Integer bookPages) {
        this.bookPages = bookPages;
    }

    /**
     * Return the book's title
     * @return book's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the book's title
     * @param title new book's title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Return book's information
     * @return a string represeting the book's information
     */
    @Override
    public String toString() {
        return "Title: " + this.title + "\n" +
                "Author: " + this.author + "\n" +
                "Pages: " + this.bookPages + "\n" +
                (this.date != null ? "Published in: " + this.date.toString() + "\n" : "") +
                "\nLocated in: " + this.path;
    }

    /**
     * Compare the current book to the given book
     * @param o book to compare
     * @return true if equals, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return path.equals(book.path);

    }

    /**
     * Return an hash code of the given object
     * @return hash code of the given object
     */
    @Override
    public int hashCode() {
        int result = author.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + path.hashCode();
        result = 31 * result + (bookPages != null ? bookPages.hashCode() : 0);
        return result;
    }
}
