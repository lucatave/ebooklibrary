package com.bitbucket.ltave;

import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.pdfbox.pdfviewer.PDFPagePanel;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class PDFViewer extends Reader {
    private PDDocument document;


    private PDFPagePanel pagePanel;

    public PDFViewer(String path) {
        this.extractInformation(path);
        this.currentPage = 0;
    }

    /**
     * Defines and initialize stage-related
     * and others UI components
     * @param primaryStage stage
     * @throws IOException
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;

        updateTitle();

        BorderPane layout = new BorderPane();

        pagePanel = new PDFPagePanel();

        // convert swing component into a javafx component
        SwingNode pagePanelNode = new SwingNode();
        createSwingContent(pagePanelNode);

        layout.setCenter(pagePanelNode);

        read(currentPage);
        updateTitle();

        layout.setVisible(true);

        primaryStage.setOnCloseRequest(event -> {
            try {
                this.close();
            } catch (IOException ignore) {
            }
        });

        Scene scene = new Scene(layout);
        this.primaryStage.setScene(scene);
        Integer height = 792;
        this.primaryStage.setHeight(height);
        Integer width = 612;
        this.primaryStage.setWidth(width);
        this.primaryStage.setResizable(false);
        this.primaryStage.requestFocus();
        this.primaryStage.show();


        setClickEvent();
        setKeyEvent();
        setScrollEvent();
    }

    /**
     * Create a swing context with a given node
     * @param node node used to create a swing context
     */
    private void createSwingContent(final SwingNode node) {
        SwingUtilities.invokeLater(() -> node.setContent(pagePanel));
    }

    /**
     * Reads the current page
     * @param currentPage sets the current page
     */
    @Override
    public void read(int currentPage) {
        this.currentPage = currentPage;

        updateTitle();

        pagePanel.setPage((PDPage) page.get(currentPage));

        pagePanel.requestFocus();

        pagePanel.repaint();
    }

    /**
     * Reads the next page
     * @throws IOException
     */
    @Override
    public void nextPage() throws IOException {
        if (currentPage < page.size() - 1) {
            currentPage++;

            this.read(currentPage);
        }
    }

    /**
     * Reads the previous page
     * @throws IOException
     */
    @Override
    public void previousPage() throws IOException {
        if (currentPage > 0) {
            currentPage--;

            this.read(currentPage);
        }
    }

    /**
     * Open the book found in path
     * @param path book's path
     * @throws IOException
     */
    @Override
    public void open(String path) throws IOException {
        if (document != null) {
            document.close();
        }

        document = PDDocument.load(new File(path));
        page = document.getDocumentCatalog().getAllPages();
    }

    /**
     * Close the opened book
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        document.close();
    }

    /**
     * Returns the pdf document
     * @return pdf document
     */
    public PDDocument getDocument() {
        return document;
    }

    /**
     * Extract information from the book found in path
     * @param path identifies the book
     * @return book with the extracted information
     */
    @Override
    public Book extractInformation(String path) {
        PDDocumentInformation metadata;
        File f = new File(path);
        try {
            if (document == null)
                open(path);
            metadata = PDDocument.load(f).getDocumentInformation();
        } catch (IOException e) {
            return null;
        }

        String author = metadata.getAuthor() != null &&
                !metadata.getAuthor().matches("^\\s+$") ?
                metadata.getAuthor() : "author";
        title = metadata.getTitle() != null &&
                !metadata.getTitle().matches("^\\s+$") ?
                metadata.getTitle() : f.getName().replaceFirst("(\\..*$)", "");
        Integer bookPages = document.getNumberOfPages();

        return new Book(author, title, null, path, bookPages);
    }
}