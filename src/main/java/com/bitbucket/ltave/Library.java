package com.bitbucket.ltave;

import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.*;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Utility class
 */
public class Library {
    private final String prefixUrl = "jdbc:sqlite:";
    private Dao<Book, String> bookDAO;
    private String databaseUrl;
    private JdbcConnectionSource jdbcConnectionSource;

    /**
     * Creates a library named "library"
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public Library() throws SQLException, ClassNotFoundException {
        this("library");
    }

    /**
     * Creates a library with the given name
     * @param libraryName library name
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public Library(String libraryName) throws SQLException, ClassNotFoundException {
        this.databaseUrl = prefixUrl + fixLibraryName(libraryName);
        establishConnection();
    }

    /**
     * Returns all the library's books
     * @return collection with all the library's book
     * @throws SQLException
     */
    public List<Book> getBooks() throws SQLException {
        return bookDAO.queryForAll();
    }

    /**
     * Establishes a connection with the library
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void establishConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");

        jdbcConnectionSource = new JdbcConnectionSource(databaseUrl);
        bookDAO = DaoManager.createDao(jdbcConnectionSource, Book.class);

        TableUtils.createTableIfNotExists(jdbcConnectionSource, Book.class);
    }

    /**
     * Closes the connection with the library
     */
    public void closeConnection() {
        jdbcConnectionSource.closeQuietly();
    }

    /**
     * Add the given book to the library
     * @param book book to be added to the library
     * @throws SQLException
     */
    public void addBook(Book book) throws SQLException {
        bookDAO.createIfNotExists(book);
    }

    /**
     * Add a list of books to the library
     * @param books list of books to be added to the library
     * @throws SQLException
     */
    private void addBooks(List<Book> books) throws SQLException {
        for (Book b : books) {
            addBook(b);
        }
    }

    /**
     * Edit the information of the given book
     * @param path book identified with path
     * @param b edited book
     * @throws SQLException
     */
    public void editBook(String path, Book b) throws SQLException {
        bookDAO.update(b);
    }

    /**
     * Returns a book with the given ID (path)
     * @param path book's location
     * @return book with the given path
     * @throws SQLException
     */
    public Book getBookById(String path) throws SQLException {
        Book retBook;
        try {
            retBook = bookDAO.queryForId(path);
        } catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();

            retBook = null;
        }

        return retBook;
    }

    /**
     * Return a book collection with the given book's metadata
     * @param book book's metadata
     * @return collection with the given metadata
     * @throws SQLException
     */
    public List<Book> getBookByArgs(Book book) throws SQLException {
        return bookDAO.queryForMatchingArgs(book);
    }

    /**
     * Return a books collection with the given book's metadata.
     *
     * A null argument equals to every possible value
     * @param path path to be found
     * @param title title to be found
     * @param date date to be found
     * @param author author to be found
     * @param bookPages number of pages to be found
     * @return collection with the given book metadata
     * @throws SQLException
     */
    public List<Book> getBookByArgs(String path, String title,
                                    Date date, String author,
                                    Integer bookPages) throws SQLException {
        return this.getBookByArgs(new Book(
                author,
                title,
                date,
                path,
                bookPages));
    }

    /**
     * Delete the given book.
     *
     * If no book is found no book will be deleted.
     * @param book book to be deleted
     * @throws SQLException
     */
    public void deleteBook(Book book) throws SQLException {
        bookDAO.delete(book);
    }

    /**
     * Fixes the library name appending accordingly ".db"
     * @param libraryName library name to be fixed
     * @return string with a consistent library name
     */
    public String fixLibraryName(String libraryName) {
        if (!libraryName.endsWith(".db")) {
            if (!libraryName.endsWith("."))
                libraryName += ".db";
            else
                libraryName += "db";
        }

        while (libraryName.startsWith(".")) {
            libraryName = libraryName.substring(1, libraryName.length());
        }


        return libraryName;
    }

    /**
     * Tries to save the old library appending
     * increasing numbers to the library name
     * @param libraryName library name to be saved
     * @return true if rename is successful, false otherwise
     */
    private boolean fixLoadedLibrary(String libraryName) {
        File willBeNewLibrary = new File(libraryName);

        File oldLibrary = new File("library.db");
        Integer suffix = 1;
        while (oldLibrary.renameTo(
                new File("library.db" + (suffix++).toString())));

        return willBeNewLibrary.renameTo(new File("library.db"));
    }

    /**
     * Cleans old libraries
     * @return true if clean operation is successful, false otherwise
     */
    public boolean cleanOldLibraries() {
        File dir = new File(".");
        File[] files = dir.listFiles((dir1, name) -> {
            return name.matches("^library\\.db(\\d)+");
        });

        boolean delete = true;
        for (File f : files)
            if (!f.delete())
                delete = false;

        return delete;
    }

    /**
     * Saves a library with the given name
     * @param libraryName library name to be saved with
     * @throws IOException
     */
    public void saveLibrary(String libraryName) throws IOException {
        Files.copy(new File("library.db").toPath(),
                new File(libraryName).toPath());
    }

    /**
     * Loads a library with the given name
     * @param libraryName name of the library to be loaded
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void loadLibrary(String libraryName)
            throws SQLException, ClassNotFoundException {
        closeConnection();

        if (!fixLoadedLibrary(libraryName)) {
            this.databaseUrl = prefixUrl + libraryName;
            establishConnection();
        }
    }

    /**
     * Exports the library in a CSV file with the given name
     * @param filename name of the CSV file
     * @throws SQLException
     * @throws IOException
     */
   public void exportToCSV(String filename) throws SQLException,
           IOException {
       if (!filename.endsWith(".csv"))
           filename += ".csv";

       FileWriter fileWriter = new FileWriter(filename);
       try (CSVWriter<Book> csvWriter = new CSVWriterBuilder<Book>(
               fileWriter).entryConverter(new CSVEntryConverter() {
           @Override
           public String[] convertEntry(Object o) {
               String[] col = new String[5];
               Book book = (Book) o;

               String path = "";
               String author = "";
               String title = "";
               String bookPages = "";
               String date = "";

               if (book.getPath() != null)
                   path = book.getPath();

               if (book.getAuthor() != null)
                   author = book.getAuthor();

               if (book.getTitle() != null)
                   title = book.getTitle();

               if (book.getBookPages() != null)
                   bookPages = book.getBookPages().toString();

               if (book.getDate() != null)
                   date = book.getDate().toString();

               col[0] = path;
               col[1] = author;
               col[2] = title;
               col[3] = bookPages;
               col[4] = date;

               return col;
           }
       }).build()) {

           csvWriter.writeAll(this.getBooks());
       }
   }
}
