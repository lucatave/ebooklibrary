package com.bitbucket.ltave;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import nl.siegmann.epublib.domain.Metadata;
import nl.siegmann.epublib.epub.EpubReader;
import nl.siegmann.epublib.viewer.Viewer;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

/**
 * Main application
 */
public class Main extends Application {

    private Menu menuFile;

    private Menu menuEdit;

    private Menu menuView;

    private Stage mainStage;
    private BorderPane layout;

    private TableView<Book> tableView;
    final private TableColumn<Book, String> path =
            new TableColumn<>("Path");
    final private TableColumn<Book, String> author =
            new TableColumn<>("Author");
    final private TableColumn<Book, String> title =
            new TableColumn<>("Title");
    final private TableColumn<Book, Date> date =
            new TableColumn<>("Date");
    final private TableColumn<Book, Integer> bookPages =
            new TableColumn<>("Pages");

    private FileChooser fileChooser;

    private String command = null;

    private Library library;

    /**
     * Setup general alert window
     * @param alert alert to setup
     * @param title title of the alert
     * @param header header of the alert
     * @return alert's return value
     */
    private Optional<ButtonType> setupMessage(Alert alert, String title, String header) {
        alert.setTitle(title);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }

    /**
     * Error alert with the given title and header
     * @param title title of the error alert
     * @param header header of the error alert
     */
    private void errorMessage(String title, String header) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        setupMessage(alert, title, header);
    }

    /**
     * Confirm message with the given title and header
     * @param title title of the confirm alert
     * @param header header of the confirm alert
     * @return alert's return value
     */
    private boolean confirmMessage(String title, String header) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        return setupMessage(alert, title, header).get() != ButtonType.CANCEL;
    }

    /**
     * Text input alert with the given title and header
     * @param title title of the given text input alert
     * @param header header of the given text input alert
     * @return alert's return value
     */
    private Optional<String> textInputMessage(String title, String header) {
        TextInputDialog textInputDialog = new TextInputDialog();
        textInputDialog.setTitle(title);
        textInputDialog.setHeaderText(header);
        return textInputDialog.showAndWait();
    }

    /**
     * Setup the extension filter for FileChooser
     */
    private void setupFileChooser() {
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All files", "*.*"),
                new FileChooser.ExtensionFilter("PDF", "*.pdf"),
                new FileChooser.ExtensionFilter("EPUB", "*.epub"),
                new FileChooser.ExtensionFilter("HTML", "*.html"),
                new FileChooser.ExtensionFilter("CSV", "*.csv"),
                new FileChooser.ExtensionFilter("Library", "*.db*")
        );
    }

    /**
     * Edit / Search form
     * @param b book parameters
     * @param buttonText text of the button
     * @param title title of the edit form
     * @param headerText header of the edit form
     * @param path true for path field, false otherwise
     * @param validation true for validation operation, false otherwise
     * @return book with the parameters of the form's field
     */
    private Optional<Book> editableForm(Book b, String buttonText,
                              String title, String headerText,
                                        boolean path, boolean validation) {
        Dialog<Book> dialog = new Dialog<>();
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);

        ButtonType customButton = new ButtonType(buttonText);
        dialog.getDialogPane().getButtonTypes().addAll(customButton, ButtonType.CANCEL);

        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setPadding(new Insets(10, 10, 10, 10));

        String mTitleAuthor =
                "^[A-Z]*[a-z]*(\\s[A-Z]*[a-z]*)*(\\s\\d*)*(\\s[A-Z]*[a-z]*)*";
        String mBookPages = "^\\d+$";

        TextField author = new TextField();
        author.setPromptText("Author");

        TextField bookTitle = new TextField();
        bookTitle.setPromptText("Title");

        DatePicker date = null;

        TextField bookPages = new TextField();
        bookPages.setPromptText("Pages number");
        if (b != null) {
            author.setText(b.getAuthor());
            bookTitle.setText(b.getTitle());

            if (b.getDate() != null)
                date = new DatePicker(LocalDate.parse(b.getDate().toString()));
            bookPages.setText(b.getBookPages().toString());
        }

        gp.add(new Label("Author:"), 0, 0);
        gp.add(author, 1, 0);

        gp.add(new Label("Title:"), 0, 1);
        gp.add(bookTitle, 1, 1);

        if (date != null) {
            gp.add(new Label("Date:"), 0, 2);
            gp.add(date, 1, 2);
        }

        gp.add(new Label("Pages number"), 0, 3);
        gp.add(bookPages, 1, 3);

        TextField textFieldPath = new TextField();
        textFieldPath.setPromptText("Path");
        if (path) {
            gp.add(new Label("Path"), 0, 4);
            gp.add(textFieldPath, 1, 4);
        }

        dialog.getDialogPane().setContent(gp);
        Platform.runLater(author::requestFocus);

        final DatePicker finalDate = date;
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == customButton) {
                String auth = null;
                String bt = null;
                String bp = null;
                String d = null;

                if (author.getText().matches(mTitleAuthor) || !validation)
                    auth = author.getText();

                if (bookTitle.getText().matches(mTitleAuthor) || !validation)
                    bt = bookTitle.getText();

                if (bookPages.getText().matches(mBookPages) || !validation)
                    bp = bookPages.getText();

                if (finalDate != null) {
                    d = finalDate.getValue().toString();
                }

                return new Book(
                        auth != null ?
                                auth.equals("") ? null : auth : null,
                        bt != null ?
                                bt.equals("") ? null : bt : null,
                        d != null ? new Date(Date.parse(d)) : null,
                        path ? textFieldPath.getText() :
                                b != null ? b.getPath() : null,
                        bp != null && !bp.equals("") ?
                                Integer.parseInt(bp) : null
                );
            }
            return null;
        });

        return dialog.showAndWait();
    }

    /**
     * Prints the library
     */
    private void printDB() {
        try {
            String filename = ".temp.csv";
            library.exportToCSV(filename);
            FileInputStream textStream = new FileInputStream(filename);
            DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
            Doc doc = new SimpleDoc(textStream, flavor, null);
            PrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
            PrintService defaultPS = PrintServiceLookup.lookupDefaultPrintService();
            if (defaultPS == null) {
                errorMessage("Error",
                        "Cannot find a valid printer");
                return;
            }
            DocPrintJob job = defaultPS.createPrintJob();
            job.print(doc, set);
        } catch (SQLException | IOException | PrintException e) {
            errorMessage("Error",
                    "An error occured while printing");
        }
    }

    /**
     * Add a directory to the library
     */
    private void addDirectory() {
        DirectoryChooser dirChooser = new DirectoryChooser();
        File file = dirChooser.showDialog(mainStage);
        if (file != null) {
            try {
                for (File f : file.listFiles())
                    library.addBook(extractBookFromFile(f));
            } catch (SQLException e) {
                errorMessage("Error",
                        "Something went wrong while adding a directory to the library");
            }
            finally {
                reloadLibrary();
            }
        }
    }

    /**
     * Sync the library with the table
     */
    private void reloadLibrary() {
        try {
            ObservableList<Book> current = tableView.getItems();
            current.setAll(library.getBooks());
        } catch (SQLException e) {
            errorMessage("Error",
                    "Something went wrong while reloading the library");
        }
    }

    /**
     * Setups the "File" menu
     */
    private void setupMenuFile() {
        menuFile = new Menu("Library");

        MenuItem menuLoadLibrary = new MenuItem("Load library");
        menuLoadLibrary.setAccelerator(KeyCombination.keyCombination("Ctrl+L"));
        menuLoadLibrary.setOnAction(event -> {
            File file = fileChooser.showOpenDialog(mainStage);
            if (file != null) {
                try {
                    library.loadLibrary(file.getName());
                    ObservableList<Book> current = tableView.getItems();
                    current.setAll(library.getBooks());
                    tableView.setItems(current);
                } catch (SQLException | ClassNotFoundException e) {
                    errorMessage("Error", "Invalid library");
                }
            }
        });

        MenuItem menuSaveLibrary = new MenuItem("Save library");
        menuSaveLibrary.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
        menuSaveLibrary.setOnAction(event -> {
            File file = fileChooser.showSaveDialog(mainStage);
            if (file != null) {
                try {
                    library.saveLibrary(file.getName());
                } catch (Exception e) {
                    errorMessage("Error", "Error while saving");
                }
            }
        });

        MenuItem menuCleanOldLibraries = new MenuItem("Clean old libraries");
        menuCleanOldLibraries.setOnAction(event -> {
            if (confirmMessage("Clean old libraries",
                    "Are you sure you want to " +
                            "PERMANENTLY DELETE ALL the old libraries?"))
                library.cleanOldLibraries();
        });

        MenuItem menuSearchBook = new MenuItem("Search book");
        menuSearchBook.setAccelerator(KeyCombination.keyCombination("Ctrl+F"));
        menuSearchBook.setOnAction(event -> {
            Optional<Book> retBook = editableForm(null, "Find", "Find book",
                    null, false, false);
            if (retBook.isPresent()) {
                try {
                    List<Book> foundBooks = library.getBookByArgs(retBook.get());
                    ObservableList<Book> current = tableView.getItems();
                    current.setAll(foundBooks);
                    tableView.setItems(current);
                } catch (SQLException e) {
                    errorMessage("Error",
                            "Something's gone wrong while searching");
                }
            }
        });

        MenuItem menuReloadLibrary = new MenuItem("Reload");
        menuReloadLibrary.setAccelerator(KeyCombination.keyCombination("Ctrl+R"));
        menuReloadLibrary.setOnAction(event -> {
            reloadLibrary();
        });

        MenuItem menuExportToCSV = new MenuItem("Export to CSV");
        menuExportToCSV.setOnAction(event -> {
            File file = fileChooser.showSaveDialog(mainStage);
            if (file != null) {
                try {
                    library.exportToCSV(file.getName());
                } catch (Exception e) {
                    errorMessage("Error", "Error while exporting to CSV");
                }
            }
        });

        MenuItem menuPrintDB = new MenuItem("Print DB");
        menuPrintDB.setAccelerator(KeyCombination.keyCombination("Ctrl+P"));
        menuPrintDB.setOnAction(event -> printDB());

        menuFile.getItems().addAll(
                menuLoadLibrary,
                menuSaveLibrary,
                menuReloadLibrary,
                menuCleanOldLibraries,
                menuSearchBook,
                menuPrintDB,
                menuExportToCSV
        );
    }

    /**
     * Setups the "Edit" menu
     */
    private void setupMenuEdit() {
        menuEdit = new Menu("Edit");

        MenuItem menuAddBook = new MenuItem("Add book");
        menuAddBook.setAccelerator(KeyCombination.keyCombination("Ctrl+A"));
        menuAddBook.setOnAction(event -> addBookForm());

        MenuItem menuAddDirectory = new MenuItem("Add directory");
        menuAddDirectory.setOnAction(event -> addDirectory());


        MenuItem menuEditBook = new MenuItem("Edit book");
        menuEditBook.setAccelerator(KeyCombination.keyCombination("Ctrl+E"));
        menuEditBook.setOnAction(event -> {
            Book b = getSelectedBook();

            Optional<Book> retBook = editableForm(b, "Edit", "Edit book",
                    null, false, true);
            if (retBook.isPresent()) {
                try {
                    library.editBook(retBook.get().getPath(), retBook.get());
                } catch (SQLException e) {
                    errorMessage("Error",
                            "Error while trying to edit.\n" +
                                    "Insert a valid Author or/and title");

                }

                ObservableList<Book> current = tableView.getItems();
                current.set(tableView.getSelectionModel().getFocusedIndex(), retBook.get());
            }
        });


        MenuItem menuDeleteBook = new MenuItem("Delete book");
        menuDeleteBook.setAccelerator(KeyCombination.keyCombination("Ctrl+D"));
        menuDeleteBook.setOnAction(event -> {
            Book selectedBook = getSelectedBook();
            if (confirmMessage("Delete",
                    "Are you sure you want to delete the book " +
                            selectedBook.getPath())) {
                try {
                    library.deleteBook(selectedBook);
                } catch (SQLException sqlException) {
                    errorMessage("Error",
                            "An error has occured while deleting the element:\n\t-" +
                                    selectedBook.getPath());
                }
            }
            ObservableList<Book> current = tableView.getItems();
            current.remove(selectedBook);
            tableView.setItems(current);
        });

        MenuItem menuSetDefaultViewer = new MenuItem("Default viewer");
        menuSetDefaultViewer.setAccelerator(KeyCombination.keyCombination("Ctrl+T"));
        menuSetDefaultViewer.setOnAction(event -> setupDefaultApplication());

        menuEdit.getItems().addAll(
                menuAddBook,
                menuAddDirectory,
                menuEditBook,
                menuDeleteBook,
                menuSetDefaultViewer);
    }


    /**
     * Setups the default application
     */
    private void setupDefaultApplication() {
        try {
            Optional<String> returned = textInputMessage("Default application",
                    "Enter the default application");

            if (returned.get() != null) {
                command = returned.get();
            }
        } catch (NoSuchElementException ignored) {
        }

        if (command == null)
            errorMessage("Error",
                    "You entered an empty command, " + "" +
                            "please enter a valid application");
    }

    /**
     * Extracts information from a PDF or EPUB file
     * @param f PDF or EPUB file
     * @return book with the extracted information
     */
    private Book extractBookFromFile(File f) {
        try {
            Book b = null;
            if (f.getName().endsWith(".pdf")) {
                PDFViewer temp = new PDFViewer(f.getPath());
                b = temp.extractInformation(f.getPath());
                temp.close();
            }
            else if (f.getName().endsWith(".epub")) {
                nl.siegmann.epublib.domain.Book book;
                EpubReader epubReader = new EpubReader();
                book = epubReader.readEpub(new FileInputStream(f));
                Metadata metadata = book.getMetadata();
                b = new Book(metadata.getAuthors().toString(),
                        metadata.getTitles().toString(),
                        null,
                        f.getPath(),
                        0);
            }

            return b;
        }
        catch (IOException e) {
            errorMessage("Error",
                    "Something went wrong while trying to add a new book");
        }

        return null;
    }

    /**
     * Adds a new book, with a FileChooser
     */
    private void addBookForm() {
        fileChooser = new FileChooser();

        File f = fileChooser.showOpenDialog(mainStage);
        try {
            if (f != null) {
                Book b = extractBookFromFile(f);
                if (b != null) {
                    library.addBook(b);
                    reloadLibrary();
                }
            }
        } catch (SQLException e) {
            errorMessage("Error",
                    "Something went wrong while trying to add a new book");
        }

    }

    /**
     * Setup the "View" menu
     */
    private void setupMenuView() {
        menuView = new Menu("View");
        MenuItem show = new MenuItem("Show");
        show.setAccelerator(KeyCombination.keyCombination("Enter"));
        show.setOnAction(event -> showBook());

        menuView.getItems().add(show);
    }

    /**
     * Setup the menu bar
     */
    private void setupMenu() {
        MenuBar menuBar = new MenuBar();
        setupMenuFile();
        setupMenuEdit();
        setupMenuView();

        menuBar.getMenus().addAll(menuFile, menuEdit, menuView);
        layout.setTop(menuBar);
    }

    /**
     * Return the selected book in the table
     * @return the selected book in the table
     */
    private Book getSelectedBook() {
        return tableView.getItems().get(
                tableView.getSelectionModel().getFocusedIndex());
    }

    /**
     * Returns true of false depending on which viewer to be used
     * @param fileType type of the book
     * @return true for internal viewer, false otherwise
     */
    private boolean useInternal(String fileType) {
        List<String> choices = new ArrayList<>();
        String internal = "Internal viewer (" +
                fileType + ")";
        String external = "External viewer (" +
                command + ")";
        choices.add(internal);
        choices.add(external);

        ChoiceDialog<String> dialog = new ChoiceDialog<>(
                internal, choices);
        dialog.setTitle("Select viewer");
        dialog.setHeaderText("Which viewer would you like to use");
        dialog.setContentText("Viewer:");
        Optional<String> result = dialog.showAndWait();

        if (!result.isPresent())
            throw new NoSuchElementException();

        return result.get().contains(fileType);

    }

    /**
     * Shows the selected book in the table
     */
    private void showBook() {
        try {
            Book selectedBook = getSelectedBook();
            if (selectedBook.getPath().endsWith(".pdf")) {
                if (command == null || useInternal("PDF")) {
                    showPDF(selectedBook);
                    return;
                }
            } else if (selectedBook.getPath().endsWith(".epub")) {
                if (command == null || useInternal("EPUB")) {
                    showEpub(selectedBook);
                    return;
                }
            }
            showDefault(selectedBook);
        } catch (NoSuchElementException ignored) {
            ignored.printStackTrace();
        }
    }

    /**
     * Show the selected PDF book
     * @param book book to be shown
     */
    private void showPDF(Book book) {
        try {
            PDFViewer pdfViewer = new PDFViewer(book.getPath());
            pdfViewer.start(new Stage());
        } catch (IOException e) {
            errorMessage("Error", "Error while loading PDF:\n\t" +
                    book.getPath());
        }
    }

    /**
     * Show the selected EPUB book
     * @param book book to be shown
     */
    private void showEpub(Book book) {
        try {
            Viewer viewer = new Viewer(
                    new EpubReader().readEpub(
                            new FileInputStream(book.getPath())));
        } catch (IOException e) {
            errorMessage("Error",
                    "Error while loading Epub:\n\t- " + book.getPath());
        }
    }

    /**
     * Show with the external viewer the selected book
     * @param book book to be shown
     */
    private void showDefault(Book book) {
        if (command == null) {
            setupDefaultApplication();
            return;
        }
        try {
            Runtime.getRuntime().exec(command + " " + book.getPath());
        } catch (IOException e) {
            errorMessage("Error",
                    "An error has occured while loading:\n" +
            book.getPath());
        }
    }

    /**
     * Setup the operations to be done while closing the application
     */
    private void setupCloseStage() {
        mainStage.setOnCloseRequest(event -> {
            boolean proceed = true;
            try {
                library.closeConnection();
            } catch (Exception e) {
                if (!confirmMessage("Error while closing",
                        "Library couldn't be closed properly, " +
                                "do you wish to proceed anyway?"))
                    proceed = false;
            } finally {
                if (proceed && confirmMessage("Exit",
                        "Do you want to quit?"))
                    mainStage.close();

                event.consume();
            }
        });
    }

    /**
     * Setup the table
     */
    private void setupTable() {
        tableView = new TableView<>();

        path.setCellValueFactory(
                new PropertyValueFactory<>("path"));
        author.setCellValueFactory(
                new PropertyValueFactory<>("author"));
        title.setCellValueFactory(
                new PropertyValueFactory<>("title"));
        date.setCellValueFactory(
                new PropertyValueFactory<>("date"));
        bookPages.setCellValueFactory(
                new PropertyValueFactory<>("bookPages"));

        tableView.getColumns().setAll(path, author, title, bookPages, date);

        try {
            ObservableList<Book> partialBooks =
                    FXCollections.observableArrayList(library.getBooks());
            tableView.setItems(partialBooks);
        } catch (SQLException sqlException) {
            errorMessage("Error while loading library",
                    "An error has occured while loading library");
        }

        tableView.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY))
                if (event.getClickCount() == 2)
                    showBook();
        });

        layout.setCenter(tableView);
    }

    /**
     * Initialize the stage and other UI things
     * @param stage initialized stage
     */
    private void setupMainStage(Stage stage) {
        mainStage = stage;
        mainStage.setTitle("EBook Library");

        layout = new BorderPane();

        setupTable();

        layout.setPadding(new Insets(0, 10, 10, 10));
        layout.autosize();

        Scene sceneMainStage = new Scene(layout);
        setupMenu();
        setupFileChooser();

        mainStage.setScene(sceneMainStage);
        setupCloseStage();
    }

    /**
     * Starts the application
     * @param primaryStage initialized stage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        library = new Library("library.db");

        setupMainStage(primaryStage);

        mainStage.show();
    }

    /**
     * Initialize Application specific members.
     * @param args arguments to be passed
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
        launch(args);
    }
}
