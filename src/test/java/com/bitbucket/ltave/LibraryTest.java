package com.bitbucket.ltave;

import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LibraryTest {
    private Library library;
    private Book book;
    private Book b;
    private List<Book> books;

    public LibraryTest() throws SQLException, ClassNotFoundException {
        String databaseUrl = "testLibrary.db";
        library = new Library(databaseUrl);

        book = new Book(
                "Bjarne Stroustrup",
                "C++",
                null,
                "/home/luca/Documents/Books/C++/Stroustrup.pdf",
                322

        );

        library.addBook(book);
        b = book;

        books = library.getBooks();

        if (books.size() < 1000) {
            for (Integer i = 0; i < 3000; i++) {
                String suffix = i.toString();
                Book tmp = new Book();
                tmp.setPath(book.getPath() + suffix);
                tmp.setAuthor(book.getAuthor() + suffix);
                tmp.setTitle(book.getTitle() + suffix);

                library.addBook(tmp);
            }
        }
    }

    @Test
    public void testFixLibraryNameSimple() {
        assertEquals("library.db", library.fixLibraryName("library"));
    }

    @Test
    public void testFixLibraryNameMedium() {
        assertEquals("library.db", library.fixLibraryName("library."));
    }

    @Test
    public void testFixLibraryNameHard1() {
        assertEquals("library.db", library.fixLibraryName(".library.db"));
    }

    @Test
    public void testFixLibraryNameHard2() {
        assertEquals("librarydb.db", library.fixLibraryName("librarydb"));
    }

    @Test
    public void testAddBook() throws Exception {
        try {
            library.addBook(book);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(book, library.getBookById(book.getPath()));
    }

    @Test
    public void testConsistency1() throws Exception {
        assertEquals(library.getBookById(book.getPath()), book);
    }

    @Test
    public void testAddBook2() throws Exception {
        try {
            library.addBook(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(b, library.getBookById(b.getPath()));
    }

    @Test
    public void testBookByArgs() throws Exception {
        assertEquals(book, library.getBookByArgs(
                null,
                "C++",
                null,
                null,
                null).get(0));
    }

    /*@Test
    public void testDeleteBook() throws Exception {
        library.deleteBook(book);
        assertEquals(null, library.getBookById(book.getPath()));
    }*/

    /*@Test
    public void testDeleteBook2() throws Exception {
        Book b2 = new Book();
        b2.setPath("fake");

        library.deleteBook(b2);

        assertEquals(null, library.getBookById(b2.getPath()));
    }*/


    @Test
    public void testUpdateBook() throws Exception {
        Book b2 = new Book(
                null,
                "title like a boss",
                null,
                "/home/luca/the/great/path/2",
                322
        );

        try {
            library.editBook(b.getPath(), b2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Book bdb = library.getBookById(b.getPath());

        if (bdb.getAuthor() != null)
            assertEquals(null, null);
        else
            assertEquals(null, new Book());

        assertEquals(b.getTitle(), bdb.getTitle());
        assertEquals(b.getPath(), bdb.getPath());
        assertEquals(b.getDate(), bdb.getDate());
        assertEquals(b.getBookPages(), bdb.getBookPages());
    }
}